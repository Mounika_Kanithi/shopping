# Generated by Django 3.1.4 on 2021-01-07 08:13

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='OrderModel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Total', models.IntegerField()),
                ('Order_placed_on', models.DateField(auto_now_add=True)),
                ('Order_updated_on', models.DateField(auto_now=True)),
                ('Status_of_order', models.CharField(choices=[('new', 'new'), ('paid', 'paid')], max_length=5)),
                ('Mode_of_payment', models.CharField(choices=[('cash_on_delivery', 'cash_on_delivery')], max_length=25)),
            ],
        ),
        migrations.CreateModel(
            name='ProductModel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Title_of_Product', models.CharField(max_length=50)),
                ('Description_of_Product', models.TextField()),
                ('Link_of_image', models.URLField()),
                ('Price_of_Product', models.FloatField()),
                ('Product_created_on', models.DateField(auto_now_add=True)),
                ('Product_updated_on', models.DateField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='OrderProductModel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Quantity_of_product', models.IntegerField()),
                ('Price', models.FloatField()),
                ('Order_Id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cart.ordermodel')),
                ('Product_Id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cart.productmodel')),
                ('User', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='ordermodel',
            name='Product',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cart.productmodel'),
        ),
        migrations.AddField(
            model_name='ordermodel',
            name='User',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
