from django.contrib import admin

# Register your models here.
from cart.models import ProductModel,OrderModel, OrderProductModel

admin.site.register(ProductModel)
admin.site.register(OrderModel)
admin.site.register(OrderProductModel)



