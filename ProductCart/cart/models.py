
from django.contrib.auth.models import User
from django.db import models


# Create your models here.
class ProductModel(models.Model):
    Title_of_Product = models.CharField(max_length=50)
    Description_of_Product = models.TextField()
    Link_of_image = models.URLField()
    Price_of_Product = models.FloatField()
    Product_created_on = models.DateField(auto_now_add=True)
    Product_updated_on = models.DateField(auto_now=True)

    def __str__(self):
        return self.Title_of_Product


STATUS_CHOICES = (
    ('new', 'new'),
    ('paid', 'paid')
)

MODE_OF_PAYMENT = (
    ('cash_on_delivery', 'cash_on_delivery'),
)


class OrderModel(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    Total = models.IntegerField()
    Order_placed_on = models.DateField(auto_now_add=True)
    Order_updated_on = models.DateField(auto_now=True)
    Status_of_order = models.CharField(choices=STATUS_CHOICES, max_length=5)
    Mode_of_payment = models.CharField(choices=MODE_OF_PAYMENT, max_length=25)
    Product = models.ForeignKey(ProductModel, on_delete=models.CASCADE)

    def __str__(self):
        return self.Product.Title_of_Product


class OrderProductModel(models.Model):
    Order_Id = models.ForeignKey(OrderModel, on_delete=models.CASCADE)
    Product_Id = models.ForeignKey(ProductModel, on_delete=models.CASCADE)
    Quantity_of_product = models.IntegerField()
    Price = models.FloatField()
    user= models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.Product_Id.Title_of_Product