from django.urls import path

from cart.views import ListOfProducts, DetailsOfProduct,ListOfOrders, DetailsOfOrders, ListOfOrderProducts,DetailsOfOrderProducts

urlpatterns = [
    path('products/', ListOfProducts.as_view({'get': 'list'}), name='listofproducts'),
    path('product/<pk>',
         DetailsOfProduct.as_view(
             {'get': 'retrieve', 'post': 'create', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
         name='detailsofproducts'),
    path('orders/', ListOfOrders.as_view({'get': 'list'}), name='listoforders'),
    path('order/<str:pk>',
         DetailsOfOrders.as_view(
             {'get': 'retrieve', 'post': 'create', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
         name='detailsoforders'),
    path('orderproduct/', ListOfOrderProducts.as_view({'get': 'list'}), name='orderproductlist'),
    path('orderproduct/<str:pk>',
         DetailsOfOrderProducts.as_view(
             {'get': 'retrieve', 'post': 'create', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
         name='detailsoforderproducts'),
]
