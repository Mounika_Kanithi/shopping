
from rest_framework import viewsets
from rest_framework import permissions
from rest_framework.exceptions import PermissionDenied
from rest_framework.filters import SearchFilter,OrderingFilter
from rest_framework.renderers import JSONRenderer, TemplateHTMLRenderer

from cart.models import ProductModel, OrderModel, OrderProductModel
from cart.serializers import ProductSerializer, OrderSerializer, OrderProductSerializer


class ListOfProducts(viewsets.ReadOnlyModelViewSet):
    queryset = ProductModel.objects.all()
    serializer_class = ProductSerializer
    search_fields = ['id', 'Title']
    filter_backends = (SearchFilter, OrderingFilter)

class DetailsOfProduct(viewsets.ModelViewSet):
    queryset = ProductModel.objects.all()
    serializer_class = ProductSerializer

class IsAuthorized(permissions.BasePermission):

    def has_object_permission(self, request, view, object):
        return object.User == request.User

class ListOfOrders(viewsets.ReadOnlyModelViewSet):
    serializer_class = OrderSerializer
    permission_classes = (IsAuthorized,)

    def get_queryset(self):
        user = self.request.user
        if user.is_authenticated:
            return OrderModel.objects.filter(user=user)
        raise PermissionDenied()

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class DetailsOfOrders(viewsets.ModelViewSet):
    serializer_class = OrderSerializer
    permission_classes = (IsAuthorized,)

    def get_queryset(self):
        user = self.request.user
        if user.is_authenticated:
            return OrderModel.objects.filter(user=user)
        raise PermissionDenied()

    def perform_create(self, serializer):
        serializer.save(User=self.request.user)


class ListOfOrderProducts(viewsets.ReadOnlyModelViewSet):
    serializer_class = OrderProductSerializer
    permission_classes = (IsAuthorized,)

    def get_queryset(self):
        user = self.request.user
        if user.is_authenticated:
            return OrderProductModel.objects.filter(user=user)
        raise PermissionDenied()

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class DetailsOfOrderProducts(viewsets.ModelViewSet):
    serializer_class = OrderProductSerializer
    permission_classes = (IsAuthorized,)

    def get_queryset(self):
        user = self.request.user
        if user.is_authenticated:
            return OrderProductModel.objects.filter(user=user)
        raise PermissionDenied()

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

